<?php

namespace Drupal\ckeditor_allow_empty_tags\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use \Drupal\ckeditor\CKEditorPluginContextualInterface;
use \Drupal\Core\Form\FormStateInterface;
use \Drupal\editor\Entity\Editor;

/**
 * Defines the "ckeditor_allow_empty_tags" plugin.
 *
 * @CKEditorPlugin(
 *   id = "ckeditor_allow_empty_tags",
 *   label = @Translation("Allow Empty Tags"),
 * )
 */
class CKEditorAllowEmptyTags extends CKEditorPluginBase implements
    CKEditorPluginConfigurableInterface,
    CKEditorPluginContextualInterface {

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {

    $settings = $editor->getSettings();

    if (isset($settings['plugins']['ckeditor_allow_empty_tags']['allow_empty'])) {
      $allow = $settings['plugins']['ckeditor_allow_empty_tags']['allow_empty']['allow'];
      $tags = $settings['plugins']['ckeditor_allow_empty_tags']['allow_empty']['tags'];
    }

    $form['allow_empty'] = array(
      '#title' => t('Allow Empty Tags'),
      '#type' => 'fieldset',
    );

    $form['allow_empty']['allow'] = array(
      '#title' => t('Allow Empty Tags'),
      '#description' => t('Check this box if you want CKEditor to allow empty tags on this text format.'),
      '#type' => 'checkbox',
      '#default_value' => isset($allow) ? $allow : 0,
    );

    $show_if_allow_empty_enabled = array(
      'visible' => array(
        ':input[name="editor[settings][plugins][ckeditor_allow_empty_tags][allow_empty][allow]"]' => array('checked' => TRUE),
      ),
    );

    $form['allow_empty']['tags'] = array(
      '#type' => 'textarea',
      '#title' => t('List Tags'),
      '#description' => t('Input all tags you want to format with empty spaces separated by a comma (For example: span,i,p,div). May not work with elements such as headings.'),
      '#states' => $show_if_allow_empty_enabled,
      '#default_value' => isset($tags) ? $tags : '',
    );

    return $form;
  }

}
