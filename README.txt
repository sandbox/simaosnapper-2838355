Drupal CKEditor Allow Empty Tags Module:
-----------------------------------------
Maintainers:
  Ian Corritore (https://www.drupal.org/user/3449797/)


Overview:
----------
CKEditor Allow Empty Tags is a module that creates a tab on text format configuration pages that allows input of specific tags that the user wants CKEditor to ignore if empty (I.e. CKEditor will not strip these tags).  This is great for developers who want to implement Font-Awesome <i> tags and similar cases.  Although a workaround can be used by placing an entity such as &nbsp; in the tag, there are cases when the developer may need it to be completely empty to avoid css workarounds that can cause rotation, origin, and other issues.


REQUIREMENTS
------------

This module requires the following modules:

 * CKEditor


CONFIGURATION
-------------

Once enabled, a tab will be available in text format configuration pages that provides an on/off checkbox that will allow empty html tags for the current text format.  The user then only needs to specify the html tags they will need to be empty separated by a comma.